﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	
	// Input axes
	public string movementAxisVertical;
	public string movementAxisHorizontal;

	public string lookAxisVertical;
	public string lookAxisHorizontal;

	public string dash;
	
	[SerializeField]
	private float moveSpeed;

	private float currentMoveSpeed;

	private Vector2 moveDirection;
	private Vector3 lookDirection;

	private float angRad;
	public float angDeg;

	// This defines the physics layers which this unit can interact with. Any layer not specified will be ignored.
	[SerializeField]
	private LayerMask layerCheck;

	[SerializeField]
	private LayerMask groundLayer;

	private Rigidbody2D thisRigidbody;

	private RaycastHit2D bottomLeftRay;
	private RaycastHit2D bottomRightRay;
	private RaycastHit2D topLeftRay;
	private RaycastHit2D topRightRay;
	
	private RaycastHit2D groundCheckLeft;
	private RaycastHit2D groundCheckRight;

	private bool isDashing = false;

	public bool isFalling = false;

	public bool isCharging = false;

	private Vector3 tempDir;
	private float dashCooldown;
	
	public bool useKeyboardControls = false;

	void Start () {
		currentMoveSpeed = moveSpeed;
		thisRigidbody = GetComponent<Rigidbody2D>();

		if (useKeyboardControls) {
			movementAxisVertical = "Vertical";
			movementAxisHorizontal = "Horizontal";
			dash = "mouse2";
		}

	}

	void Update () {

		if (isCharging && currentMoveSpeed > 1.5) {
			currentMoveSpeed -= Time.deltaTime;
		}
		else if (currentMoveSpeed != moveSpeed) {
			currentMoveSpeed = moveSpeed;
		}


		//TODO : finish implementation of falling & resetting
		// check if there is ground under the player, adjust the gravity accordingly.
		groundCheckLeft = Physics2D.Raycast(transform.position + new Vector3(-0.3f, -0.5f, 0), Vector2.down, 0.05f, groundLayer);
		groundCheckRight = Physics2D.Raycast(transform.position + new Vector3(0.3f, -0.5f, 0), Vector2.down, 0.05f, groundLayer);

		if (groundCheckLeft.collider == null && groundCheckRight.collider == null && !isDashing) {
			isFalling = true;
			thisRigidbody.gravityScale = 5;
		}

		if (!isFalling) {
			thisRigidbody.gravityScale = 0;
			thisRigidbody.velocity = Vector2.zero;
		}

		// handle rotation
		if (useKeyboardControls) {

			Vector3 v = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

			lookDirection = new Vector2 (v.x - transform.position.x, v.y - transform.position.y);
				
			angRad = Mathf.Atan2 (lookDirection.y, lookDirection.x);
			angDeg = Mathf.Rad2Deg * angRad;
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angDeg));
		}

		if (!useKeyboardControls && (Mathf.Abs(Input.GetAxis(lookAxisHorizontal)) > 0.2f || Mathf.Abs(Input.GetAxis(lookAxisVertical)) > 0.2f)) {
			
			// using get axis for a smoother looking turn
			lookDirection = new Vector2 (Input.GetAxis(lookAxisHorizontal), Input.GetAxis(lookAxisVertical));
				
			angRad = Mathf.Atan2 (lookDirection.y, lookDirection.x);
			angDeg = Mathf.Rad2Deg * angRad;
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angDeg));
		}

		if (dashCooldown > 0) {
			dashCooldown -= Time.deltaTime;
			if (dashCooldown > 5) {
				transform.Translate(tempDir*20*Time.deltaTime, Space.World);
			}
			else {
				isDashing = false;
			}
		}
		else {
			isDashing = false;
		}

		if (dashCooldown <= 0 && Input.GetButtonDown(dash)){

			tempDir = lookDirection.normalized;
			dashCooldown = 5.15f;
			isDashing = true;
		}


		// Movement axes, catering for joysticks. Defining dead zones. 
		// Axes arew defined in Edit > Project Settings > Input
		if (Input.GetAxisRaw (movementAxisHorizontal) > 0.2f) {
			moveDirection.x = 1;
		}
		else if (Input.GetAxisRaw (movementAxisHorizontal) < -0.2f) {
			moveDirection.x = -1;
		}
		else {
			moveDirection.x = 0;
		}

		if (Input.GetAxisRaw (movementAxisVertical) > 0.2f) {
			moveDirection.y = 1;
		}
		else if (Input.GetAxisRaw (movementAxisVertical) < -0.2f) {
			moveDirection.y = -1;
		}
		else {
			moveDirection.y = 0;
		}

		if(moveDirection != Vector2.zero && !isDashing) {
			Move();
		}
	}

	void Move() {
		
		// Handle terrain collisions
		// Raycast in move direction
		if (moveDirection.x > 0) {
			bottomRightRay = Physics2D.Raycast(transform.position + new Vector3(0.5f, -0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
			topRightRay = Physics2D.Raycast(transform.position + new Vector3(0.5f, 0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
		}
		else if (moveDirection.x < 0) {
			bottomLeftRay = Physics2D.Raycast(transform.position + new Vector3(-0.5f, -0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
			topLeftRay = Physics2D.Raycast(transform.position + new Vector3(-0.5f, 0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
		}


		if (moveDirection.y > 0) {
			topLeftRay = Physics2D.Raycast(transform.position + new Vector3(-0.5f, 0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
			topRightRay = Physics2D.Raycast(transform.position + new Vector3(0.5f, 0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
		}
		else if (moveDirection.y < 0) {
			bottomLeftRay = Physics2D.Raycast(transform.position + new Vector3(-0.5f, -0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
			bottomRightRay = Physics2D.Raycast(transform.position + new Vector3(0.5f, -0.5f, 0), moveDirection, currentMoveSpeed*Time.deltaTime, layerCheck);
		}

		// if no ray returns a collider, move normally
		if (topLeftRay.collider == null && topRightRay.collider == null && bottomLeftRay.collider == null && bottomRightRay.collider == null) {
			transform.Translate(moveDirection.normalized*currentMoveSpeed*Time.deltaTime, Space.World);
			return;
		}

		// Otherwise, move very close to the collider, without actually colliding.
		if (topLeftRay.collider != null){
			transform.Translate(moveDirection.normalized*(topLeftRay.distance - 0.05f), Space.World);
		}
		if (topRightRay.collider != null){
			transform.Translate(moveDirection.normalized*(topRightRay.distance - 0.05f), Space.World);
		}
		if (bottomLeftRay.collider != null){
			transform.Translate(moveDirection.normalized*(bottomLeftRay.distance - 0.05f), Space.World);
		}
		if (bottomRightRay.collider != null){
			transform.Translate(moveDirection.normalized*(bottomRightRay.distance - 0.05f), Space.World);
		}
	}
}
