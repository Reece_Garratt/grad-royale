﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

	[SerializeField]
	private float [] reloadTimers;

	public float simpleReloadRemaining;
	public float chargeReloadRemaining;
	public float shotgunReloadRemaining;

	public string fireAxis;
	public string gun0;
	public string gun1;
	public string gun2;

	private float chargeTimeRemaining = 0;

	public GameObject [] bullets;

	public int selectedIndex = 0;
	
	private bool useKeyboardControls = false;

	void Start () {
		useKeyboardControls = GetComponent<Movement>().useKeyboardControls;

		if (useKeyboardControls) {
			fireAxis = "mouse1";
		}
	}
	void Update () {
		
		if(simpleReloadRemaining > 0){
			simpleReloadRemaining -= Time.deltaTime;
		}
		if(chargeReloadRemaining > 0){
			chargeReloadRemaining -= Time.deltaTime;
		}
		if(shotgunReloadRemaining > 0){
			shotgunReloadRemaining -= Time.deltaTime;
		}
		if (chargeTimeRemaining > 0) {
			chargeTimeRemaining -= Time.deltaTime;
		}

		//button input for select bullet here

		switch (selectedIndex) {
			case 0:
				if(Input.GetButton(fireAxis) && simpleReloadRemaining <= 0) {
					shoot();
				}
				break;
			case 1:
				if(Input.GetButtonDown(fireAxis) && chargeReloadRemaining <= 0) {
					chargeShot();
				}
				if(Input.GetButtonUp(fireAxis) && chargeReloadRemaining <= 0) {
					shootCharged();
				}
				break;
			case 2: 
				if(Input.GetButton(fireAxis) && shotgunReloadRemaining <= 0) {
					shootShotgun();
				}
				break;
		}

		if (Input.GetButton(gun0) || (useKeyboardControls && Input.GetKeyDown(KeyCode.Alpha1))) {
			selectBullet(0);
		}
		if (Input.GetButton(gun1) || (useKeyboardControls && Input.GetKeyDown(KeyCode.Alpha2))) {
			selectBullet(1);
		}
		if (Input.GetButton(gun2)  || (useKeyboardControls && Input.GetKeyDown(KeyCode.Alpha3))) {
			selectBullet(2);
		}
	}

	public void selectBullet(int n) {
		if (selectedIndex == n) {
			return;
		}
		selectedIndex = n;
	}

	void shoot () {
		Object.Instantiate(bullets[selectedIndex], transform.position + transform.right* 0.8f, transform.rotation);
		
		simpleReloadRemaining = reloadTimers[selectedIndex];

		//play sound based on index
	}

	void shootShotgun () {
		Object.Instantiate(bullets[selectedIndex], transform.position + transform.right* 0.8f, transform.rotation);
		shotgunReloadRemaining = reloadTimers [selectedIndex];
	}

	void chargeShot () {
		chargeTimeRemaining = 2.5f;
		GetComponent<Movement>().isCharging = true;
	}

	void shootCharged () {

		GetComponent<Movement>().isCharging = false;
		Debug.Log("chargeTimeRemaining");
		Debug.Log(chargeTimeRemaining);
		
		if(chargeTimeRemaining <= 0.4f) {
			Object.Instantiate(bullets[selectedIndex], transform.position + transform.right* 0.8f, transform.rotation);
		}
	}
}
