﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoundManager : MonoBehaviour {

    public bool controlsEnabled; 
    public GameObject ground;
    private List<GameObject> groundTiles;
    public PlayerManager player1, player2;
    public int p1Wins = 0, p2Wins = 0;
    public int MaxRounds = 3;
    public float time;
    public Sprite p1WinSprite;
    public Sprite p2WinSprite;
    public SpriteRenderer WinSplash;

    bool GameOver = false;
    float diff;
	// Use this for initialization
	void Start () {
        WinSplash.enabled = false;
        diff = time;
        groundTiles = ground.GetComponent<GroundManager>().children;
    }
	
	// Update is called once per frame
	void Update () {

        if (GameOver)
            ShowWinSplash();

        if (player1.isDead())
        {
            p2Wins++;
            if (groundTiles != null && groundTiles.Count > 0) {
                player1.reset(groundTiles[3*groundTiles.Count/4].transform.position);
            }
            else {
                player1.reset(player1.startingPosition);
            }
            player1.GetComponentsInChildren<LayerManager>()[0].ResetTerrain();
        }

        if (player2.isDead())
        {
            p1Wins++;
            if (groundTiles != null && groundTiles.Count > 0) {
                player2.reset(groundTiles[3*groundTiles.Count/4].transform.position);
            }
            else {
                player2.reset(player2.startingPosition);
            }
            player2.GetComponentsInChildren<LayerManager>()[0].ResetTerrain();
        }

        GameOver = p1Wins > MaxRounds / 2 || p2Wins > MaxRounds / 2;
            
        if (GameOver)
        {
            player1.disableControls();
            player2.disableControls();

            if (p1Wins > p2Wins)
                PlayerWon(player1, 1);
            else if (p2Wins > p1Wins)
                PlayerWon(player2, 2);
        }
	}

    public void RestartCurrentScene()
    {
        player1.enableControls();
        player2.enableControls();
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    void ShowWinSplash()
    {
        diff -= Time.deltaTime % 60;

        WinSplash.color = new Color(1f, 1f, 1f, diff / (time + 1));

        if (diff <= 0)
            RestartCurrentScene();
    }

    void PlayerWon(PlayerManager winner, int no)
    {
        WinSplash.GetComponent<SpriteRenderer>().sprite = (no == 1) ? p1WinSprite : p2WinSprite;
        WinSplash.enabled = true;
        Debug.Log("Game won!  P1 : " + p1Wins + " P2 : " + p2Wins);
    }
}
