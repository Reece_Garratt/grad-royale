﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerManager : MonoBehaviour {

    private PlayerManager player;
    private Movement movement;
    private SpriteRenderer spriteRenderer;

    static bool switched = false;

    // Use this for initialization
    void Start () {
        player = GetComponent<PlayerManager>();
        movement = GetComponent<Movement>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update () {
        if (player.isDead()) {
            ResetTerrain();
        }
            
        if (movement.isFalling && !switched)
        {
            if (transform.position.y > -6.375f) {
                float diff = -6.375f - transform.position.y;
                int layersAbove = (int)Mathf.Ceil(diff/1.5f);

                spriteRenderer.sortingOrder = 1+2*layersAbove;
            }
            switched = true;
        }
    }

    public void ResetTerrain()
    {
        spriteRenderer.sortingOrder = 1;
        switched = false;
    }
}
