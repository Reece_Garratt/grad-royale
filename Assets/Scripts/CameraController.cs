﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	private float shakeTimeRemaining;
	private Vector3 startingPosition;

	void Start() {
		startingPosition = transform.position;	
	}
	void Update() {
		if (shakeTimeRemaining > 0) {
			shakeTimeRemaining -= Time.deltaTime;
			Vector2 shakePosition = Random.insideUnitCircle*0.1f;
			transform.position = new Vector3 (transform.position.x + shakePosition.x, transform.position.y + shakePosition.y, transform.position.z);
		}
		else {
			transform.position = startingPosition;
		}
	}

	public void ScreenShake (float time) {
		shakeTimeRemaining = time;
	}
}
