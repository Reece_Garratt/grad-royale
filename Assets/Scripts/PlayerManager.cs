﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {

	public Vector3 startingPosition;
    private bool died = false;
    
	//public int health

	private int roundsWon = 0;
	public Text scoreText; 

	public GameObject cam;

	void Start () {
		startingPosition = transform.position;
		scoreText.text = "WON: " + roundsWon;
	}
	
	void Update () {
		if (transform.position.y < -15) {
			cam.GetComponent<CameraController>().ScreenShake(0.4f);
            died = true;
           // reset(); now done from RoundManager if death is detected
		}
		if (!GetComponent<Movement>().isFalling) {
			GetComponent<Rigidbody2D>().velocity = Vector3.zero;
		}
	}

	public void reset (Vector3 pos) {
		GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        GetComponent<Movement>().isFalling = false;
		transform.position = pos;
        died = false;
    }

    public void SubtractHealth() {

		//health --;
		//if (health <= 0) {
			//end the round
		//}

		cam.GetComponent<CameraController>().ScreenShake(0.2f);
		died = true;
	}
        
	public void enableControls () {
		GetComponent<Movement>().enabled = true;
		GetComponent<Shooting>().enabled = true;
	}
	public void disableControls () {
		GetComponent<Movement>().enabled = false;
		GetComponent<Shooting>().enabled = false;
	}

    public bool isDead()
    {
        return died;
    }

	public void increaseRoundsWon() {
		roundsWon++;
		scoreText.text = "WON: " + roundsWon;
	}
}
