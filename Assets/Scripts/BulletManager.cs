﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour {

	public float speed;
	public float destroyTimer = 4;
	public bool isPlayer1Bullet;
	public bool isShotGun = false;

	void Start () {
		GetComponent<Rigidbody2D> ().AddRelativeForce (new Vector2(speed, 0));
		Destroy (this.gameObject, destroyTimer);
	}
	void Update() {
		if(isShotGun){
			transform.localScale += new Vector3(0, 8*Time.deltaTime, 0);
		}	
	}

	void OnCollisionEnter2D(Collision2D other) {
		if(isPlayer1Bullet) {
			if (other.collider.gameObject.tag == "Player2") {
				other.collider.gameObject.GetComponent<PlayerManager>().SubtractHealth();
			}
		}
		else {
			if (other.collider.gameObject.tag == "Player1") {
				other.collider.gameObject.GetComponent<PlayerManager>().SubtractHealth();
			}
		}

		Destroy (this.gameObject);
	}
}
