﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour {

	private float timer = 0;
	private float totalTime;
	Collider2D [] col;
	public List<GameObject> children;

	Vector3 originalPosition;
	float yPos = 0;
	GameObject spriteChild;

	int index = -1;
	bool removing = false;
	// Use this for initialization
	void Start () {
		col = GetComponentsInChildren<Collider2D>();
		children = new List<GameObject>();
		foreach(Collider2D c in col) {
			children.Add(c.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		totalTime += Time.deltaTime;
		yPos = Mathf.Sin(20*totalTime)*0.15f;

		if (timer > 0) {
			timer -= Time.deltaTime;

			if (spriteChild != null) {
				if (timer > 1){
					spriteChild.transform.position = new Vector3 (originalPosition.x, originalPosition.y+yPos, originalPosition.z);
				}
				else {
					children[index].transform.Translate(Vector2.down*25*Time.deltaTime);
				}
			}
		}
		else {
			removeGround();
		}
	}
	
	void removeGround () {
		removing = true;
		if (children.Count > 2) {
			index = Random.Range(0, children.Count-1);
		}
		if (index >= 0){
			spriteChild = children[index].GetComponentInChildren<SpriteRenderer>().gameObject;
			originalPosition = spriteChild.transform.position;
			Destroy(children[index], 5);
		}
		timer = 3;
	}
}
