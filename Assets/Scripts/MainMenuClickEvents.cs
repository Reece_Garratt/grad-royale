﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuClickEvents : MonoBehaviour {

	public void	loadByIndex(int sceneIndex) {
		// load scence using scene manager
		SceneManager.LoadScene(sceneIndex);
	}

	public void quiteGame() {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
}
