﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputDetector : MonoBehaviour {

	public Color active;
	public Color passive;

	public bool raw = false;
	public bool getButton = false;


	public string axis;
	
	private SpriteRenderer sp;

	void Start() {
		sp = GetComponent<SpriteRenderer>();

		if (getButton) {
			raw = false;
		}
		if (raw) {
			getButton = false;
		}
	}
	
	void Update () {
		if(raw) {
			if (Mathf.Abs(Input.GetAxisRaw(axis)) > 0) {
				sp.color = active;
			}
			else {
				sp.color = passive;
			}
		}
		else if (getButton) {
			if (Input.GetButton(axis)) {
				sp.color = active;
			}
			else {
				sp.color = passive;
			}
		}
		else {
			if (Mathf.Abs(Input.GetAxis(axis)) > 0) {
				Debug.Log("getAxis : " + Input.GetAxis(axis) );
				sp.color = active;
			}
			else {
				sp.color = passive;
			}
		}
	}
}
